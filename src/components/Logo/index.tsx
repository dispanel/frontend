import * as React from 'react'
import './style.scss';

export class Logo extends React.Component {
  public render() {
    return (
      <div className='logo'>
        <header className='clip'>
          <h1>Dispanel</h1>
        </header>
      </div>
    );
  }
}
