import * as React from 'react';
import './style.scss';

export class SidePanel extends React.Component {
  public render() {
    return (
      <div className='sidepanel'>
        {this.props.children}
      </div>
    );
  }
}