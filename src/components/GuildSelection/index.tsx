import * as React from 'react';
import * as request from 'superagent';
import './style.scss';

export class GuildSelection extends React.Component<{}, { serverName: string, hovered: boolean }> {
  public constructor(props: any) {
    super(props);
    this.state = {serverName: props.serverName || 'No server name', hovered: false};
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this); 
  }

  public async componentDidMount() {
    const responseName = await request
      .get('http://localhost:3000/v1/guilds/427884195394879499/settings/name');
    const serverName: string = responseName.text as unknown as string;
    this.setState({serverName});
  }

  public render() {
    const { serverName } = this.state;
    return (
      <div className='guild-selection' onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
        <h2>
          <text>{serverName}</text>
          <span id='arrow'>&#8964;</span>
        </h2>
        <ul className={this.state.hovered ? 'hovered': ''}>
          <li>Another Server</li>
          <li>Sample</li>
          <li>TPH</li>
        </ul>
      </div>
    );
  }

  private onMouseEnter(event: React.MouseEvent<HTMLDivElement>) {
    this.setState({hovered: true});
  }
  private onMouseLeave(event: React.MouseEvent<HTMLDivElement>) {
    this.setState({hovered: false});
  }
}