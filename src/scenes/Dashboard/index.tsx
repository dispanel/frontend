import * as React from 'react';
import { Categories } from 'src/components/Categories';
import { GuildSelection } from 'src/components/GuildSelection';
import { Logo } from 'src/components/Logo';
import { SidePanel } from 'src/components/SidePanel';
import './style.scss';

export class Dashboard extends React.Component<{name: string}, {serverName: string}> {
  public render() {
    return (
      <div className='dashboard'>
        <div className='container'>
          <Logo />
          <div className='panel'>
            <SidePanel>
              <GuildSelection />
              <Categories />
            </SidePanel>
          </div>
        </div>
      </div>
    );
  }
}
