import * as React from 'react';
import * as request from 'superagent';
import './style.scss';

interface ICategory {
  id: string;
  name: string;
  rawPosition: number;
}

interface IState {
  categories: ICategory[];
  draggedCategoryID: string;
  currentActiveCategoryIndex: number;
  guildID: string;
}

export class Categories extends React.Component<{}, IState> {
  public constructor(props: any) {
    super(props);
    this.state = { 
      categories: [], 
      currentActiveCategoryIndex: -1, 
      draggedCategoryID: '', 
      guildID: '427884195394879499' 
    };
    this.onDrag = this.onDrag.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this); 
    this.onDragLeave = this.onDragLeave.bind(this);
    this.onDragOver = this.onDragOver.bind(this);
    this.onDragStart = this.onDragStart.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.addCategory = this.addCategory.bind(this);
    this.deleteCategory = this.deleteCategory.bind(this);
  }

  public async componentDidMount() {
    const responseCategories = await request
      .get('http://localhost:3000/v1/guilds/'+ this.state.guildID + '/settings/categories');
    const categories: ICategory[] = responseCategories.body;
    this.setState({categories});
  }

  /* tslint:disable:no-console */

  public render() {
    const categories = this.getCategories();

    return (
      <div className='categories'>
        <header>
          <h2>Categories</h2>
        </header>
        {categories}
    </div>
    )
  }

  private getCategories() {
    const {categories} = this.state;
    categories.sort((a, b) => a.rawPosition - b.rawPosition);
    const categoriesList = (
      <ul>
        {categories.map(
            (category: ICategory) => 
              (
                <li 
                  key={category.name} 
                  draggable={true} 
                  onDrag={this.onDrag} 
                  onDragStart={this.onDragStart}
                  onDragEnd={this.onDragEnd}
                  onDragOver={this.onDragOver}
                  onDragLeave={this.onDragLeave}
                  onDrop={this.onDrop}
                  id={category.id}>
                    <input type='text' className='category-name' value={category.name} />
                    <span className='controls'>
                      <button className='controls-delete' onClick={this.deleteCategory}>🗑</button>
                    </span>
                </li>
              )
          )}
      </ul>
    );

    return (
      <div className='categories'>
        <div className='list-adder'>
          <input type='field' placeholder='New category' onKeyDown={this.addCategory}/>
        </div>
        <div className='list'>
          {categoriesList}
        </div>
      </div>
    );
  }

  private changeCategoryPosition(selectedCategoryID: string, dropOnCategoryID: string) {
    const { categories } = this.state;
    const selectedCategory: ICategory | undefined = categories.find((category) => category.id === selectedCategoryID);
    const dropOnCategory: ICategory | undefined = categories.find((category) => category.id === dropOnCategoryID);
    let newCategories = categories;
    if (selectedCategory && dropOnCategory) {
      // Change the in between the selected and dropped on category positions
      if (selectedCategory.rawPosition < dropOnCategory.rawPosition) {
        newCategories = newCategories.map(category => {
          if (category.rawPosition === selectedCategory.rawPosition) {
            // Change the selected category's position to the dropped on one's
            return { ...selectedCategory, rawPosition: dropOnCategory.rawPosition };
          } else if (category.rawPosition > selectedCategory.rawPosition 
                     && category.rawPosition <= dropOnCategory.rawPosition) {
            // Everything in between should get one position upwards
            return { ...category, rawPosition: category.rawPosition - 1 };
          } else {
            return category;
          }
        });
      } else if (selectedCategory.rawPosition > dropOnCategory.rawPosition) {
        newCategories = newCategories.map(category => {
          if (category.rawPosition === selectedCategory.rawPosition) {
            // Change the selected category's position to the dropped on one's
            return { ...selectedCategory, rawPosition: dropOnCategory.rawPosition };
          } else if (category.rawPosition < selectedCategory.rawPosition 
                     && category.rawPosition >= dropOnCategory.rawPosition) {
            // Everything in between should get one position downwards
            return { ...category, rawPosition: category.rawPosition + 1 };
          } else {
            return category;
          }
        });
      }

      // Update category positions
      this.setState({categories: newCategories});

      // Send selected position's position to the server
      request
        .put('http://localhost:3000/v1/guilds/'+ this.state.guildID + '/settings/categories/'+selectedCategory.id)
        .send({position: dropOnCategory.rawPosition})
        .then(val => {/** */});
    }
  }

  private onDragStart(event: React.DragEvent<HTMLLIElement>): void {
    event.dataTransfer.setData('text/plain', '');

    const currentTargetID = event.currentTarget.id;
    this.setState({draggedCategoryID: currentTargetID});
    event.currentTarget.classList.add('on-drag');
    const crt = document.createElement('div');
    crt.innerHTML = "<h1>Americano</h1>";
    crt.setAttribute('style', 'position: absolute; color: #ee2141; background-color: white;');
    const crtImage = new Image();
    crtImage.innerHTML = "<h1>Americano</h1>";
    crtImage.setAttribute('style', 'position: absolute; color: #ee2141; background-color: white;');

    
    // event.dataTransfer.setDragImage(crtImage, 0, 0);
  }
  private onDrag(event: React.DragEvent<HTMLLIElement>): void {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }
  private onDragEnd(event: React.DragEvent<HTMLLIElement>): void {
    event.currentTarget.classList.remove('on-drag');
  }
  private onDragOver(event: React.DragEvent<HTMLLIElement>): void {
    event.preventDefault();
    event.stopPropagation();

    const currentCategory = this.state.categories.find(category => category.id === event.currentTarget.id);
    const draggedCategory = this.state.categories.find(category => category.id === this.state.draggedCategoryID);
    if (currentCategory && draggedCategory) {
      if (currentCategory.rawPosition <= draggedCategory.rawPosition) {
        event.currentTarget.classList.add('on-drag-over-upper-elements');
      } else {
        event.currentTarget.classList.add('on-drag-over-lower-elements');
      }
    }
  }
  private onDragLeave(event: React.DragEvent<HTMLLIElement>): void {
    event.currentTarget.classList.remove('on-drag-over-upper-elements', 'on-drag-over-lower-elements');
  }
  private onDrop(event: React.DragEvent<HTMLLIElement>): void {
    event.stopPropagation();
    event.currentTarget.classList.remove('on-drag-over-upper-elements', 'on-drag-over-lower-elements');
    const { draggedCategoryID } = this.state;
    const droppedOnCategoryID = event.currentTarget.id;
    this.changeCategoryPosition(draggedCategoryID, droppedOnCategoryID);
  }
  private async addCategory(event: React.KeyboardEvent<HTMLInputElement>): Promise<void> {
    if (event.key!=='Enter') {
      return;
    }
    const categoryName = event.currentTarget.value || '';
    event.currentTarget.value = '';
    const { guildID } = this.state;
    try {
      const response = await request
        .post('http://localhost:3000/v1/guilds/'+guildID+'/settings/categories')
        .send({name: categoryName});
      const body: { title: string, reference: string} = response.body;
      const { categories } = this.state;
      const newCategoryPosition = categories.sort((a, b) => b.rawPosition - a.rawPosition)[0].rawPosition || 0;
      const newCategories = categories;
      newCategories.push({id: body.reference, name: categoryName, rawPosition: newCategoryPosition});
      this.setState({categories: newCategories});
      
    } catch (e) {
      throw new Error(e);
    }
  }
  private deleteCategory(event: React.MouseEvent<HTMLButtonElement>): void {
    const button = event.currentTarget;
    const span: HTMLSpanElement = button.parentElement as HTMLSpanElement;
    const li: HTMLLIElement = span.parentElement as HTMLLIElement;
    const id = li.id;
    const { guildID } = this.state;
    request
      .delete('http://localhost:3000/v1/guilds/'+guildID+'/settings/categories/'+id)
      .then();
    const { categories } = this.state;
    const newCategories = categories.filter(category => category.id !== id);
    this.setState({categories: newCategories});
  }
}