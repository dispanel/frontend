import * as React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.scss';
import { Dashboard } from './scenes/Dashboard';
import { Landpage } from './scenes/Landpage';
import { Login } from './scenes/Login';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route path="/" exact={true} component={Landpage} />
            <Route path="/login" component={Login} />
            <Route path="/dashboard" component={Dashboard} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
