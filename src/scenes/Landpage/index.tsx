import * as React from 'react';
import './style.scss';

export class Landpage extends React.Component {
  public loginOnClickHandler(e: React.MouseEvent<HTMLButtonElement>): void | undefined {
    e.preventDefault();
    window.open(`https://discordapp.com/api/oauth2/authorize?client_id=516432181317468161&permissions=8&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Fv1%2Fapp%2Fdiscord-callback&scope=bot`,
                  'Connect to Discord',
                  `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=450,height=600`);
  }

  public render() {
    return (
      <div className='landpage'>
        <div className='header'>
          <header>
            <h1>Displet</h1>
            <button onClick={this.loginOnClickHandler}>Login</button>
          </header>
        </div>
      </div>
    );
  }
}
